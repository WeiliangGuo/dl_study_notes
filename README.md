## Here I collect supplementary materials and keep extra notes while studying through [deep learning courses](https://www.deeplearning.ai/)

### [Course 1: Neural Networks and Deep Learning](https://gitlab.com/WeiliangGuo/dl_study_notes/wikis/Course1)

### [Course 2: Improving Deep Neural Networks](https://gitlab.com/WeiliangGuo/dl_study_notes/wikis/course2)


This project was migrated to:
https://github.com/WeiliangGuo/deepleaning_studies